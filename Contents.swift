//: Playground - noun: a place where people can play

import UIKit

var firstWord = Array("Sunday")   // Write first word here
let secondWord = Array("Saturday") // Write second word here
var options = String()

firstWordArrayLoopForI: for a in firstWord {
    secondWordArrayLoopForI: for b in secondWord {
        if a == b && secondWord.index(of: b)! >= firstWord.index(of: a)! && secondWord.index(of: b)! <= secondWord.count && firstWord.index(of: a)! <= firstWord.count {
            continue firstWordArrayLoopForI
        }
    }
    if firstWord.count < secondWord.count {
        options += "i"
    }
}

firstWordArrayLoopForS: for a in firstWord {
    secondWordArrayLoopForS: for b in secondWord {
        if a == b {
            continue firstWordArrayLoopForS
        }
    }
    options += "s"
}

firstWordArrayLoopForD: for a in firstWord {
    secondWordArrayLoopForD: for b in secondWord {
        if firstWord.index(of: a)! > secondWord.index(of: b)! && secondWord.index(of: b)! < secondWord.count && firstWord.index(of: a)! < firstWord.count {
            continue firstWordArrayLoopForD
        }
    }
    if firstWord.count > secondWord.count {
        options += "d"
    }
}
print(options) //That's the answer!
